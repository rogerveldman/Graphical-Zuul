package GUI;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;

import Core.Room;

/**
 * @author Roger Veldman
 */
public class GUI {
	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Guess we are going with java's defualt.");
		}
		
		JTextArea output = new JTextArea(20, 70);
		JTextField input = new JTextField();
		output.setEditable(false);
		GUIGame game = new GUIGame(output);
		game.welcome();
		input.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));

		JFrame frame = new JFrame();
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JScrollPane jsp = new JScrollPane(output);
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		CustomPanel draw = new CustomPanel(game.getRoot());
		panel.add(jsp, BorderLayout.NORTH);
		panel.add(jsp, BorderLayout.CENTER);
		panel.add(input, BorderLayout.SOUTH);
		frame.add(panel);
		frame.pack();

		input.requestFocus();

		input.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				game.play(input.getText().trim());
				input.setText("");
				output.setCaretPosition(output.getText().length() - 1);
				Room room = game.getPlayerRoom();
				draw.setDrawOffset(room, draw.getSize().width, draw.getSize().height);
				input.requestFocus();

			}
		});
		JFrame viewer = new JFrame();
		viewer.add(draw);
		viewer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		viewer.setSize(300, 300);
		viewer.setResizable(true);
		viewer.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent evt) {
				Room room = game.getPlayerRoom();
				draw.setDrawOffset(room, draw.getSize().width, draw.getSize().height);
			}
		});

		output.setLineWrap(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		viewer.setLocation(frame.getX() - viewer.getWidth(), frame.getY());
		viewer.setVisible(true);
	}
}
